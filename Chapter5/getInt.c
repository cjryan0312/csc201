#include <ctype.h>
#include <stdio.h>
#include "acctools.h"

#define MAXLINE 1000

int getch(void);
void ungetch(int);


main() {
    char line[MAXLINE];
    accgetline(line, MAXLINE);

    int num = getint(line);
    printf("%d \n", num);
}

int getint(int *pn) {
    int c, sign;

    while (isspace(c = getch()));

    if (!isdigit(c) && c != EOF && c != '+' && c != '-') {
        ungetch(c);
        return 0;
    }
    sign = (c == '-') ? -1 : 1;
    if (c == '+' || c == '-')
        c = getch();
    for (*pn = 0; isdigit(c); c = getch())
        *pn = 10 * *pn + (c - '0');
    *pn *= sign;
    if (c != EOF)
        ungetch(c);
    return c;
}
