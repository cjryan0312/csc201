#include <stdio.h>

#define MAXLINE 1000

int get_line(char s[], int lim);
void reverse(char input[], char result[]);

main() {
    int len;
    char line[MAXLINE];
    char result[MAXLINE];

    while ((len = get_line(line, MAXLINE)) > 0) {
        if (len > 0) {
            reverse(line, result);
            printf("%s \n", result);
        }
    }
    return 0;
}

int get_line(char s[], int lim) {
    int c, i;

    for(i = 0; i < lim - 1 && (c = getchar()) != EOF && c != '\n'; ++i) {
        s[i] = c;
    }
    if (c == '\n'){
        s[i] = c;
        ++i;
    }
    s[i] = '\0';
    return i;
}

void reverse(char s[], char reversed_line[]) {
    int i, c, max;

    i = 0;

    while(s[i] != '\n') {
        i++;
    }

    max = i;
    for(c = 0; c <= max; ++c) {
        reversed_line[c] = s[i];
        --i;
    }
}
