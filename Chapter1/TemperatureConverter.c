#include <stdio.h>

main() {
    float fhar, celsius;
    int lower, upper, step;

    lower = 0;
    upper = 300;
    step = 20;

    fhar = lower;
    printf("Fahrenheit To Celsius Table \n");
    while (fhar <= upper) {
        celsius = 5.0 * (fhar - 32.0) / 9.0;
        printf("%3.0f %6.1f\n", fhar, celsius);
        fhar = fhar + step;
    }
}
