#include <stdio.h>

#define MAXLINE 1000

void reverse_line(char input[], int len,char reversed_line[]);

main() {
    int len;
    char line[MAXLINE];
    char reversed_line[MAXLINE];

    while (getline(&line, &len, stdin) > 0) {
        if (len > 1) {
            reverse_line(line, len, reversed_line);
            printf("%s \n", reversed_line);
        }
    }
    return 0;
}

void reverse_line(char s[], int len, char reversed_line[]) {
    for(int c = len; c >= 0; --c) {
        reversed_line[c] = s[c];
    }
}
