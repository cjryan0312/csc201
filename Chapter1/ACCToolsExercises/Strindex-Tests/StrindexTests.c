#include <stdio.h>
#include "acctools.h"

#define MAXLINE 1000

main() {
    char line[MAXLINE];
    int middle;
    int beginning;
    int end;
    int same_string;
    int non_existant;
    accgetline(line, MAXLINE);

    // test 1, t occurs in the middle of s.
    middle = strindex(line, "middle");
    printf("in the test file, the middle is at index %d \n", middle);

    // test 2, t occurs at the beginning of s.
    beginning = strindex(line, "beginning");
    printf("in the test file, the beginning is at index %d \n", beginning);

    // test 3, t occurs at the end of s.
    end = strindex(line, "end");
    printf("in the test file, the end is at index %d \n", end);

    // test 4, t and s are the same string.
    same_string = strindex(line, "beginning of the sentence, middle of the sentence, or the sentence end?");
    printf("in the test file, the entire string index is: %d \n", same_string);

    // test 5, t does not occur in s.
    non_existant = strindex(line, "I do not exist");
    printf("in the test file, the index does not exist at %d \n", non_existant);
}
