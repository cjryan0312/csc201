#include <stdio.h>
#include "acctools.h"

#define MAXLINE 1000

main() {
    int len;
    int non_existant;
    char line[MAXLINE];

    while ((len = accgetline(line, MAXLINE)) > 0) {
        if (len > 0) {
            non_existant = strindex(line, "I do not exist");
            printf("in the test file, the index does not exist at %d \n", non_existant);
        }
    }
    return 0;
}
