#include <stdio.h>
#include "acctools.h"

#define MAXLINE 1000

main() {
    int len;
    int beginning;
    char line[MAXLINE];

    while ((len = accgetline(line, MAXLINE)) > 0) {
        if (len > 0) {
            beginning = strindex(line, "beginning");
            printf("in the test file, the beginning is at index %d \n", beginning);
        }
    }
    return 0;
}
