#include <stdio.h>
#include "acctools.h"

#define MAXLINE 1000

main() {
    int len;
    int middle;
    char line[MAXLINE];

    while ((len = accgetline(line, MAXLINE)) > 0) {
        if (len > 0) {
            middle = strindex(line, "middle");
            printf("in the test file, the middle is at index %d \n", middle);
        }
    }
    return 0;
}
