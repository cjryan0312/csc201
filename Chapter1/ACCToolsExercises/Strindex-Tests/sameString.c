#include <stdio.h>
#include "acctools.h"

#define MAXLINE 1000

main() {
    int len;
    int same_string;
    char line[MAXLINE];

    while ((len = accgetline(line, MAXLINE)) > 0) {
        if (len > 0) {
            same_string = strindex(line, "beginning of the sentence, middle of the sentence, or the sentence end?");
            printf("in the test file, the entire string index is: %d \n", same_string);
        }
    }
    return 0;
}
