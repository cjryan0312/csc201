#include <stdio.h>
#include "acctools.h"

#define MAXLINE 1000

main() {
    int len;
    int end;
    char line[MAXLINE];

    while ((len = accgetline(line, MAXLINE)) > 0) {
        if (len > 0) {
            end = strindex(line, "end");
            printf("in the test file, the end is at index %d \n", end);
        }
    }
    return 0;
}
