#include <stdio.h>
#include "acctools.h"

#define MAXLINE 1000

void reverse(char input[], char result[]);

main() {
    int len;
    char line[MAXLINE];
    char result[MAXLINE];

    while ((len = accgetline(line, MAXLINE)) > 0) {
        if (len > 0) {
            reverse(line, result);
            printf("%s \n", result);
        }
    }
    return 0;
}

void reverse(char s[], char reversed_line[]) {
    int i, c, max;

    i = strindex(s, '\n');

    max = i;
    for(c = 0; c <= max; ++c) {
        reversed_line[c] = s[i];
        --i;
    }
}
