#include <stdio.h>
#include "acctools.h"

#define MAXLINE 1000

void copy(char to[], char from[]);

main() {
    int len;
    int max;
    char line[MAXLINE];
    char longest[MAXLINE];

    max = 0;
    while ((len = accgetline(line, MAXLINE)) > 0) {
        if (len > 0) {
            printf("%s \n", line);
            if (len > max) {
                max = len;
                copy(longest, line);
            }
        }
    }
    if (max > 0) {
        printf("the longest line was: \"%s\", with a length of %d", longest, max);
    }
    return 0;
}

void copy(char to[], char from[]) {
    int i;

    i = 0;
    while ((to[i] = from[i]) != '\0') {
        i++;
    }
}
