#include <stdio.h>

#define LOWER 0
#define UPPER 300
#define STEP 20

float convertCelsius(int m);

main() {
    int i;

    for(i = LOWER; i <= UPPER; i = i + STEP) {
        printf("%3d %6.1f\n", i, convertCelsius(i));
    }
    return 0;
}
float convertCelsius(int fahr) {
    float p;

    p = (5.0/9.0) * (fahr - 32);
    return p;
}
