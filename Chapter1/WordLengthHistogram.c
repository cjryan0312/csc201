#include <stdio.h>

#define MAXLENGTH 39

main() {
    int c, i, t, nLength;
    int wordLength[MAXLENGTH];
    nLength = c = i = t = 0;

    for (i = 0; i < MAXLENGTH; i++) {
        wordLength[i] = 0;
    }

    while ((c = getchar()) != EOF) {
        if (c != ' ' && c != '\n' && c != '\t') {
            nLength++;
        }

        else if (c != ',' && c != '.' && c != ':' && c != '-') {
            nLength++;
        }

        else {
            ++wordLength[nLength];
            nLength = 0;
        }
    }
    ++wordLength[nLength];

    printf("Word Length Histogram\n");
    for (i = 1; i < MAXLENGTH; ++i) {
        printf("%2d: ", i);
        for(t = 0; t < wordLength[i]; ++t) {
            putchar('*');
        }
        putchar('\n');
    }
}
