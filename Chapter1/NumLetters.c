#include <stdio.h>

main() {
    int i, c, t;
    int letters[26];

    for (i = 0; i < 26; i++) {
        letters[i] = 0;
    }

    while ((c = getchar()) != EOF) {
        if(c > 64 && c < 91) {
            ++letters[c - 65];
        }
        else if(c > 96 && c < 123) {
            ++letters[c - 97];
        }
    }
    printf("Number Of Characters: ");
    for (i = 0; i < 26; i++) {
        printf("%c%c: %d \n", i + 65, i + 97, letters[i]);
    }

}
