#include <stdio.h>

int main() {
    int a, b;
    char c;
    printf("Enter two numbers and a char: ");
    scanf("%d %d %c", &a, &b, &c);

    printf("\nSum: %d. c as char: %c. c as hex/int: %d", a+b, c, c);


    return 0;
}
