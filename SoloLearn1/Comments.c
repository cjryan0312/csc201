#include <stdio.h>

/* This is a
* comment that spans
* multiple lines
*/

//this is a single line comment
int main(void) {
	printf("Check the source code for comments");
	return 0;
}
